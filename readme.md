# Requirements

* nodejs

## Install

```bash
npm install resumed jsonresume-theme-github
```

## Create a sample resume (will create the resume.json file)

```bash
npx resumed init
```

## Or Import from your LinkedIn Profile using this Chrome extension (will create the resume.json file)

https://chrome.google.com/webstore/detail/json-resume-exporter/caobgmmcpklomkcckaenhjlokpmfbdec

## Render Resume  (create the resume.html file)

```bash
npx resumed render --theme github
```

## For the French version this little hack will do for now (create the resume.html file)

```bash
mv jsonresume-theme-github-fr node_modules/
npx resumed render --theme github-fr
```

## Workflow
```bash
rm -r node_modules/jsonresume-theme-github-fr/
cp -r jsonresume-theme-github-fr node_modules/
npx resumed render --theme github-fr
```
or
```bash
rm -r node_modules/jsonresume-theme-github-fr/ && cp -r jsonresume-theme-github-fr node_modules/ && npx resumed render --theme github-fr
```